angular.module('hdAutoComplete', [])
	.directive('hdAutoComplete', [function(){
		String.prototype.contains = function(it) { return this.indexOf(it) != -1; };
		
		var parseNgOptions=function(str){
			//item.value as item.label for item in values
			var retValue = {};
			var nameValueExp = "";
			var nameValue = {};
			var optionList = str.split("in")[1].trim();
			var selectionExp = str.split("in")[0].trim();
			if(selectionExp.contains("for"))
			{
				var itemName = selectionExp.split("for")[1].trim();
				nameValueExp = selectionExp.split("for")[0].trim();
				nameValue = parseNameValue(nameValueExp, itemName);
			}
			else nameValue = parseNameValue(selectionExp);
			retValue.options = optionList;
			retValue.namePart = nameValue.name;
			retValue.valuePart = nameValue.value;
			
			return retValue;
		};
		var parseNameValue = function(nameValueExp, itemName) {
			var retValue = {};
			
			if(nameValueExp.contains('as'))
			{
				retValue.value = nameValueExp.split('as')[0].trim();
				retValue.name = nameValueExp.split('as')[1].trim();
			}
			else {
				retValue.value = nameValueExp.trim();
				retValue.name = nameValueExp.trim();
			}
			
			if(itemName)
			{
				retValue.name = retValue.name.replace(itemName + ".", "")
				retValue.value = retValue.value.replace(itemName + ".", "")
			}
			return retValue;
			
		};
		
		return{
			replace: true,
			templateUrl: 'hd-autocompleteTemplate.htm',
			require: 'ngModel',
			ristrict : 'E',
			link : function($scope, element, attributes, ngModel){
				var optionObject = parseNgOptions(attributes.ngOptions);
				var options = $scope[optionObject.options];
				var selectObject = _(options).map(function(item) {return { label: item[optionObject.namePart], value: item[optionObject.valuePart] }});
				element.find('input').autocomplete({
					source : selectObject,
					select: function( event, ui ) {
						ngModel.$setViewValue(ui.item.value);
						element.find('input').val(ui.item.label);
					}
				});
			}
		};
	}])

