angular.module('autocompleteDemo',['hdAutoComplete'])
	.controller('main', ['$scope',function($scope){
		$scope.values = [
					 {"value": 1, "label" :  "ActionScript"},
					 {"value": 2, "label" :  "AppleScript"},
					 {"value": 3, "label" :  "Asp"},
					 {"value": 4, "label" :  "BASIC"},
					 {"value": 5, "label" :  "C"},
					 {"value": 6, "label" :  "C++"},
					 {"value": 7, "label" :  "Clojure"},
					 {"value": 8, "label" :  "COBOL"},
					 {"value": 9, "label" :  "ColdFusion"},
					 {"value": 10, "label" :  "Erlang"},
					 {"value": 11, "label" :  "Fortran"},
					 {"value": 12, "label" :  "Groovy"},
					 {"value": 13, "label" :  "Haskell"},
					 {"value": 14, "label" :  "Java"},
					 {"value": 15, "label" :  "JavaScript"},
					 {"value": 16, "label" :  "Lisp"},
					 {"value": 17, "label" :  "Perl"},
					 {"value": 18, "label" :  "PHP"},
					 {"value": 19, "label" :  "Python"},
					 {"value": 20, "label" :  "Ruby"},
					 {"value": 21, "label" :  "Scala"},
					 {"value": 22, "label" :  "Scheme"}
					];
		
	}])